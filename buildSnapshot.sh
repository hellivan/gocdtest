#!/bin/sh

PROJECT=`cat ./package.json |grep name|cut -d '"' -f4`
VERSION=`cat ./package.json |grep version|cut -d '"' -f4`
COMMIT=`git rev-parse --short HEAD`

#git describe --tags --exact-match  HEAD

IMAGE_NAME=${IMAGE_NAME:-${PROJECT}}
TAG="${VERSION}-${COMMIT}"

echo "Building image '${IMAGE_NAME}' with tag '${TAG}'"
docker build --force-rm=true -t ${IMAGE_NAME}:${TAG} ./
